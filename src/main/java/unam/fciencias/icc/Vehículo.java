package unam.fciencias.icc;

public interface Vehículo {

  public int getMarcha();

  public void subeVelocidad (int velocidad);

  public void bajaVelocidad (int velocidad);

  public double rendimiento (double distancia, double litros);
 
  public int distanciaRecorrida (int inicial, int terminal);
}
